using Microsoft.VisualStudio.TestTools.UnitTesting;
using HipChat.Api.Models;
using System.Threading.Tasks;
using FluentAssertions;

namespace HipChat.Api.Test
{
    //using fulentAssertions for easier to read
    [TestClass]
    public class ParserUnitTests
    {
        [TestMethod]
        public async Task Parser_Constructor_WhenInitialized_ReturnsMentionCountOne()
        {
            var message = "This has one emoticon @john";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.mentions.Count.Should().Be(1);
        }
        #region mentions
        [TestMethod]
        public async Task ParseMessageAsync_GetMentions_WhenNonExist_ReturnsNull()
        {
            var message = "There are no mentions in this text. ? (text in here)";
            var parser = await Parser.CreateParserAsync(message);
          
            parser.ParsedResults.mentions.ShouldBeEquivalentTo(null);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetMentions_WhenThreeExists_ReturnsCountThree()
        {
            var message = "This is the base test. @John @Bob and maybe @danny";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.mentions.Count.Should().Be(3);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetMentions_WhenItsFirstWord_ReturnsCountOne()
        {
            var message = "@chris you around ?";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.mentions.Count.Should().Be(1);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetMentions_WhenEmbededDoesNotCount_ReturnsCountThree()
        {
            var message = "This is withembededintextrightnow@embeded . @John @Bob and maybe @danny";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.mentions.Count.Should().Be(3);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetMentions_WhenEndCharIsNotAlphaNotWhiteSpace_ReturnsWithoutChar()
        {
            var message = "This isNonWordCharacterEnding @John_works @Bob) and maybe @danny";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.mentions[1].Should().Be("Bob");   
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetMentions_WhenEndCharIsNotAlphaNotWhiteSpace_ReturnsMentionsCountThree()
        {
            var message = "This isNonWordCharacterEnding @John_works @Bob) and maybe @danny";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.mentions.Count.Should().Be(3);
            
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetMentions_WhenUnderScoreInMention_ReturnsMention()
        {
            var message = "This isNonWordCharacterEnding @John_works @Bob) and maybe @danny";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.mentions[0].Should().Be("John_works");

        }
        [TestMethod]
        public async Task ParseMessageAsync_GetMentions_WhenUserRepeated_ReturnsNonIncrementedCount()
        {
            var message = "This is repeates @John @Bob and maybe @danny  @John";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.mentions.Count.Should().Be(3);
        }
        #endregion
        #region emoticons
        [TestMethod]
        public async Task ParseMessageAsync_GetEmoticons_WhenNoneExist_ReturnsNull()
        {
            var message = "There are no emoticons in this text. ? @test here";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.emoticons.ShouldBeEquivalentTo(null);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetEmoticons_WhenMultiple_ReturnsCountTwo()
        {
            var message = "Good morning! (megusta) (coffee)";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.emoticons.Count.Should().Be(2);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetEmoticons_WhenEmoticonFirstWord_ReturnsCountTwo()
        {
            var message = "(one) sdfs (two) text. ? @test here";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.emoticons.Count.Should().Be(2);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetEmoticons_WhenSpaceInBeween_ReturnsCountOne()
        {
            var message = "There are  emoticons in this (one space) sdfs (two) text. ? @test here";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.emoticons.Count.Should().Be(1);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetEmoticons_WhenEmbedded_ReturnsFalse()
        { 
            var message = "There are no emoticons in this(dontfindthis)sdfs text. ? @test here";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.emoticons.ShouldBeEquivalentTo(null);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetEmoticons_WhenLongerThanFifteenChars_ReturnsFalse()
        {
            var message = "There are  emoticons in this (thisisoverfifteencharacterslongforsure) sdfs text. ? @test here";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.emoticons.ShouldBeEquivalentTo(null);
        }

        #endregion
        #region links
        [TestMethod]
        public async Task ParseMessageAsync_GetLinks_WhenNoneExist_ReturnsNull()
        {
            var message = "There are no emoticons in this text. ? @test here (no links here)";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.links.ShouldBeEquivalentTo(null);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetLinks_WhenEndsWithNoSpace_ReturnsTrue()
        {
            var message = "Olympics are starting soon; http://www.nbcolympics.com";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.links.Count.Should().Be(1);
        }

        [TestMethod]
        public async Task ParseMessageAsync_GetLinks_WhenLinksFirstChar_ReturnsCountOne()
        {
            var message = "https://twitter.com/jdorfman/status/430511497475670016 and here";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.links.Count.Should().Be(1);
        }

        [TestMethod]
        public async Task ParseMessageAsync_GetLinks_WhenEmbeded_ReturnsFalse()
        {
            var message = "There are  links in this (one space) sdfshttps://twitter.com/jdorfman/status/430511497475670016(two) text. ? @test here";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.links.ShouldBeEquivalentTo(null);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetLinks_WhenValidLink_ReturnsTitle()
        {
            var expected =
                "Justin Dorfman; on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;";
            var message = "There are  links in this (one space) sdfs  https://twitter.com/jdorfman/status/430511497475670016 (two) text. ? @test here";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.links[0].title.Should().Be(expected);
        }
        [TestMethod]
        public async Task ParseMessageAsync_GetLinks_WhenInValidLink_ReturnsNotAValidPage()
        {
            var expected = "Not a valid webpage";
            var message = "There are  links in this (one space) sdfs  https://twidstter.com/jdorfman/status/430511497475670016 (two) text. ? @test here";
            var parser = await Parser.CreateParserAsync(message);

            parser.ParsedResults.links[0].title.Should().Be(expected);
        }
        #endregion
    }
}
