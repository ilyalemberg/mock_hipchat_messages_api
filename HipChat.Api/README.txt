Using .NET Core 1.1
-api should run
-tests can be ran
-I used Visual studio IIS express to run

Test call:
http://localhost:61131/api/messages/parsed?message=@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016

Expected Result
{
    "mentions": [
        "bob",
        "john"
    ],
    "emoticons": [
        "success"
    ],
    "links": [
        {
            "url": "https://twitter.com/jdorfman/status/430511497475670016",
            "title": "Justin Dorfman; on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
        }
    ]
}
