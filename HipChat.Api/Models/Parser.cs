﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace HipChat.Api.Models
{
    public class Parser
    {
        public List<string> OriginalWordList { get; private set; }
        public ParsedResults ParsedResults { get; private set; }

        private const int MaxEmoticonSize = 15;
        private readonly Dictionary<string, int> _mentionsDictionary = new Dictionary<string, int>();
        private readonly Dictionary<string, int> _emoticonsDictionary = new Dictionary<string, int>();
        private readonly List<Link> _linksList = new List<Link>();

        private Parser(string inputMessage)
        {
            OriginalWordList = !string.IsNullOrEmpty(inputMessage) ? inputMessage.Split(' ').ToList() : new List<string>();
        }
        private async Task<Parser> ParseMessageAsync()
        {
            foreach (var word in OriginalWordList)
            {
                EvaluateMention(word);
                EvaluateEmoticon(word);
                await EvaluateLinkAsync(word);
            }
            ParsedResults = new ParsedResults(_mentionsDictionary, _emoticonsDictionary, _linksList);

            return this;
        }
        /// <summary>
        /// factory pattern to account for async method not allowed in constructor
        /// </summary>
        /// <param name="inputMessage"></param>
        /// <returns></returns>
        public static async Task<Parser> CreateParserAsync(string inputMessage)
        {
            var newParserClass = new Parser(inputMessage);

            return await newParserClass.ParseMessageAsync();
        }
        private void EvaluateMention(string word)
        {
            if (word.StartsWith("@"))
            {
                var mention = word.Split('@')[1];

                if (!mention.All(c => char.IsLetterOrDigit(c) || c == '_'))
                {
                    for (var i = 0; i < mention.Length; i++)
                    {
                        if (!char.IsLetterOrDigit(mention[i]) && mention[i] != '_')
                        {
                            mention = mention.Substring(0, i);
                            break;
                        }
                    }
                }

                if (_mentionsDictionary.ContainsKey(mention))
                    _mentionsDictionary[mention] = _mentionsDictionary[mention] + 1;
                else
                    _mentionsDictionary.Add(mention, 1);
            }
        }
        private void EvaluateEmoticon(string word)
        {
            if (word.StartsWith("(") && word.EndsWith(")"))
            {
                //remove paranthesis
                var emoticon = word.Split('(')[1].Substring(0, word.Length - 2);
                if (emoticon.Length <= MaxEmoticonSize && emoticon.All(char.IsLetterOrDigit))
                {
                    if (_emoticonsDictionary.ContainsKey(emoticon))
                        _emoticonsDictionary[emoticon] = _emoticonsDictionary[emoticon] + 1;
                    else
                        _emoticonsDictionary.Add(emoticon, 1);
                }
            }
        }
        private async Task EvaluateLinkAsync(string word)
        {
            Uri uriResult;
            var result = Uri.TryCreate(word, UriKind.Absolute, out uriResult);

            if (result && (uriResult.Scheme.ToLower() == "http" || uriResult.Scheme.ToLower() == "https"))
            {
                var title = string.Empty;

                try
                {
                    var web = new HtmlWeb();
                    var doc = await web.LoadFromWebAsync(word);
                    title = doc.DocumentNode.ChildNodes["html"].ChildNodes["head"].ChildNodes["title"].InnerHtml;
                }
                catch (Exception)
                {
                    //log error here
                    title = "Not a valid webpage";
                }
                finally
                {
                    var link = new Link
                    {
                        url = word,
                        title = title
                    };

                    _linksList.Add(link);
                }
            }
        }
    }
}
