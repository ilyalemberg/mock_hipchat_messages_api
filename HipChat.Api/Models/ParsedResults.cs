﻿using System.Collections.Generic;
using System.Linq;

namespace HipChat.Api.Models
{
    public class ParsedResults
    {
        public List<string> mentions { get; set; }
        public List<string> emoticons { get; set; }
        public List<Link> links { get; set; }

        //Using nulls for JSON serialization purposes
        public ParsedResults(Dictionary<string,int> mentionsList = null, Dictionary<string, int> emoticonsList = null, List<Link> linksList = null)
        {
            mentions = (mentionsList != null && mentionsList.Count > 0) ? mentionsList.Keys.ToList() : null;
            emoticons = (emoticonsList != null && emoticonsList.Count > 0) ? emoticonsList.Keys.ToList() : null;
            links = (linksList != null && linksList.Count > 0) ? linksList : null;
        }
    }
}
