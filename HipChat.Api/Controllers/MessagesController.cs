﻿using System;
using System.Threading.Tasks;
using HipChat.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace HipChat.Api.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : Controller
    {
        /// <summary>
        /// GET api/messages/parsed?message-[message text here]
        /// </summary>
        /// <param name="message">passed in string</param>
        /// <returns>JSON object containing arrays of all matches parsed from the input string.</returns>
        [Route("parsed/", Name = "ParsedMessage")]
        [HttpGet]
        public async Task<IActionResult> GetParsedMessagedAsync([Bind(Prefix = "message")]string message)
        {
            try
            {
                //converted to async for the url title call
                var parser = await Parser.CreateParserAsync(message);
                return Ok(parser.ParsedResults);
            }
            catch (Exception ex)
            {
                //log error
                return BadRequest(ex.Message);
            }
        }
    }
}
